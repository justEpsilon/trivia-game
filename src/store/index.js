import Vue from 'vue'
import Vuex from 'vuex'
import { TriviaAPI } from '../components/Trivia/TriviaAPI'

//import {TrviaAPI}

Vue.use(Vuex)

//---------------------------------------------------------
//Example of the results array object in the API
//---------------------------------------------------------
//{"category":"Entertainment: Video Games",
//"type":"multiple",
//"difficulty":"medium",
//"question":"When was the Sega Genesis released in Japan?",
//"correct_answer":"October 29, 1988",

//"incorrect_answers":["August 14, 1989","November 30, 1990","September 1, 1986"]}
//---------------------------------------------------------

//Example URL for fetching: 
//"https://opentdb.com/api.php?amount=10&category=22&difficulty=easy"


export default new Vuex.Store({
    // the state will be updated when user inputs all these things, 
    state: {
        
        difficulty:null,
        amount:NaN,
        category:0,             // setting this to num, but will need to
        triviaCategories:[],    // fetch it from category file trough user input
                                // maybe another object for chosen input. idk. 
       
                    
        trivia:[],
        currentId: 0,
        selectedAnswer:[],
        error:''
    },

    mutations: {
        setDifficulty: (state, payload) => {
            state.difficulty = payload
        },
        setAmount: (state, payload) => {
            state.amount = payload
        },
        setCategory: (state, payload) => {
            state.category = payload
        },
        setTrivia: (state, payload) => {
            state.trivia = payload
        },
        incrementCurrentId: (state ) => {
            state.currentId += 1
            console.log(state.currentId)
        },
        setTriviaCategories: (state, payload) => {
            state.triviaCategories = payload
        },
        setSelectedAnswer: (state, payload) => {
            if (state.currentId >= state.selectedAnswer.length){ //if the anwer was not selected lenght will be id + 1 or zero in the first case.
                state.selectedAnswer.push(payload)
            } else {
                state.selectedAnswer[state.currentId] = payload 
            }
            console.log(state.selectedAnswer)
            
        },
        setError: (state, payload) => {
            state.error = payload
        },
        reset: state => {
            state.trivia = []
            state.currentId = 0
            state.selectedAnswer = [] 
            state.amount = NaN
            state.difficulty = null
            state.category = 0
        }
    },
    getters: {
        getCategories: state => {
           return state.triviaCategories
        },
        getTriviaDisplay:  state => {

            let trivia = state.trivia
            let i = state.currentId
            console.log(trivia)

            if (i < trivia.length) {
                let answerArray = [...trivia[i].incorrect_answers]
                let correct = (trivia[i].correct_answer)
                answerArray.push(correct)

                let shuffled = answerArray.sort((a, b) => a.localeCompare(b));
                    // sort if alphabetically, it is random in this case 
                return  {
                    id: i,
                    question: trivia[i].question,
                    correct_answer: trivia[i].correct_answer,
                    shuffledAnswers: shuffled
                }
            } else {
                return {
                    id:i,
                    question: '',
                    correct_answer: '',
                    shuffledAnswers: []
                }
            }  
        },
        getShuffledAnswers: (state, getters)=>{ //could merge it with trivia display and use getTriviaDisplay.shuffledanswers in html. - no time rn. 

            const trivia = state.trivia
            console.log(trivia)
            if (trivia.length == 0 || getters.areWeDone) {
                return []
            }
            console.log(getters.getTriviaDisplay.shuffledAnswers)
            return getters.getTriviaDisplay.shuffledAnswers
            
        },
        getResults: state => {

            let results = []
            for (let i = 0; state.trivia.length > i; ++i){
                let question = state.trivia[i].question
                let correctAnswer = state.trivia[i].correct_answer
                let selectedAnswer = state.selectedAnswer[i] 
                let result = {
                    id: i,
                    question: question,
                    correctAnswer: correctAnswer,
                    selectedAnswer: selectedAnswer
                }
                results.push(result)
            }
            return results
        },
        getScore: (state, getters)  => {
            let score = 0;
            for (const result of getters.getResults){ //if i use "in" instead of "of" in creates a astring of a key. BTW: undifined is appearently equal to undifined
                if (result.correctAnswer === result.selectedAnswer){
                    score ++
                    
                    console.log(score, result.correctAnswer, result.selectedAnswer, result) 
                }

            }
            return {
                score:score,
                total: state.trivia.length
            }
        },
        areWeDone: state => {
            
            return state.trivia.length <= state.currentId 
        },
        isQuestionAnswered: state => { 
            return state.currentId < state.selectedAnswer.length

        }

    },
    actions: {

        async fetchTriviaCategories({ commit }){

            try {
                const triviaCategories = await TriviaAPI.fetchTriviaCategories()
                console.log(triviaCategories)
                commit('setTriviaCategories', triviaCategories)    
            } catch (error) {
                commit('setError', error.message)
            }
            
        },
        
        async fetchTrivia({ commit, state }){
            let { amount } = state
            const { difficulty } = state 
            const { category } = state 


            let API_suffix = ''

            if (amount == amount && amount != 0) {
                API_suffix +=`amount=${amount}`

            } else {         //default number of questions set to 10. 
                amount = 10;
                API_suffix +=`amount=${amount}`
            }

            if (difficulty) {
                API_suffix +=`&difficulty=${difficulty}`
            }

            if (category != 0) {
                API_suffix += `&category=${category}`
            }
            // this is for just multiple choise &type=multiple
            // true false: &type=boolean

            API_suffix += '&type=multiple' //for sake of being late, i will only make logic for muultiple choise

            try {
                const trivia = await TriviaAPI.fetchTrivia(API_suffix)
                commit('setTrivia', trivia)
                console.log(trivia.length)

            } catch (error) {
                commit('setError', error.message)
            }
        }
    }
})