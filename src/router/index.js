import Vue from "vue"
import VueRouter from "vue-router"

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Select',
        component: () => import(/* webpackChunkName: "Login" */'../components/Trivia/Select.vue') //lazy loading the components
    },
    {
        path: '/trivia',
        name: 'Trivia',
        component: () => import(/* webpackChunkName: "Login" */'../components/Trivia/Trivia.vue') //lazy loading the components
    },
    {
        path: '/results',
        name: 'Results',
        component: () => import(/* webpackChunkName: "Login" */'../components/Trivia/Results.vue') //lazy loading the components
    }
]

///all the routs mapped to a specific end point, and this is pretty much it. 

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router;