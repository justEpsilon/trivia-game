import { constants } from "../constants"

//Example URL for fetching: 
//"https://opentdb.com/api.php?amount=10&category=22&difficulty=easy"


export const TriviaAPI = {
    
    // The url must be generated somehow: 
    fetchTrivia(API_suffix) {

       return fetch(constants.API_TRIVIA_ROOT + API_suffix)
       .then(response => response.json()
       .then(data => data.results)) //check later if this is so.
    },
    
    fetchTriviaCategories() {

        return fetch("https://opentdb.com/api_category.php")
        .then(response => response.json())
        .then(data => data.trivia_categories)
    },
}

//I could fetch trivia categories instead of having category file,
// but it was useful for me to just look at it when writting, therefore i decided to do it that way. 