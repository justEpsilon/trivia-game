import Vue from 'vue'
import router from './router'
import App from './App.vue'
import store from './store'

//import Vuex from 'vuex' need here? 


Vue.config.productionTip = false

new Vue({
  store, 
  router,
  render: h => h(App),
}).$mount('#app')
